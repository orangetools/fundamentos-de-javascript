var emmanuel = {
    nombre: 'Emmanuel',
    apellido: 'Martínez',
    edad: 32
}

var dario = {
    nombre: 'Darío',
    apellido: 'Salvat',
    ead: 25
}  

// Esta forma de desglosar el objeto dentro del atributo de la función es una característica de 
// las nuevas versiones de JavaScript
function desglosandoObjetos({ nombre }) {
    var n = nombre.toUpperCase();
    console.log(n)
}
desglosandoObjetos(emmanuel)
desglosandoObjetos({nombre: 'Juan'})

// Las nuevas versiones de JavaScript, también nos permiten utilizar esta sintaxis 
// dentro de las funciones: 

function desglosandoObjetos2(persona) {
    //var nombre = persona.nombre (Esto es lo mismo que la línea que sigue)
    var { nombre } = persona
    console.log(nombre.toUpperCase())
}

desglosandoObjetos2(emmanuel)
desglosandoObjetos2({nombre: 'Juan'})